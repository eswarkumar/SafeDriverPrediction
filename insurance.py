import h2o
import pandas as pd
from h2o.automl import H2OAutoML
h2o.init()
train = h2o.import_file('D:/Data Science/Project2/Insurance/train.csv')
test = h2o.import_file('D:/Data Science/Project2/Insurance/test.csv')
sub_data = h2o.import_file('D:/Data Science/Project2/Insurance/sample_submission.csv')
y = 'target'
x = train.columns
x.remove(y)
## Time to run the experiment
run_automl_for_seconds = 6
## Running AML for 4 Hours

aml = H2OAutoML(max_runtime_secs =run_automl_for_seconds)
train_final, valid = train.split_frame(ratios=[0.9])
print "Running H20"
aml.train(x=x, y =y, training_frame=train_final, validation_frame=valid)
print "Running H20 Completed"
leader_model = aml.leader
pred = leader_model.predict(test_data=test)
pred_pd = pred.as_data_frame()
sub = sub_data.as_data_frame()
sub['target'] = pred_pd
sub.to_csv('D:/Data Science/Project2/Insurance/pythonResult.csv', header=True, index=False)
